@extends('template.generique')

@section('contenuHeader')
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">Liste des produits <span class="badge badge-info right">{{count($product)}}</span>

        </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Liste des produits</li>
        </ol>
        </div><!-- /.col -->
    </div>
@endsection

@section('contenu')
<div class="row">
    <div class="col-12">
      <!-- /.card -->

      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Liste des produits</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Nom Produit</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($product as $product)
                    <tr>
                        <td>{{$product->libelle}}</td>
                        <td>
                        </td>
                    </tr>
                @endforeach

            </tbody>
            <tfoot>
            <tr>
                <th>Nom Produit</th>
                <th>Actions</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
@endsection



@section('js')
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    });
</script>
@endsection





