@extends('template.generique')

@section('contenuHeader')
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">Ajouter un administrateur</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Ajouter un  administrateur</li>
        </ol>
        </div><!-- /.col -->
    </div>
@endsection

@section('contenu')
<form action="{{route('addingProduct')}}" method="POST"  enctype="multipart/form-data">
    @csrf
    <div class="col-lg-12">

        <div class="form-panel">
          <div class=" form">
            <form class="cmxform form-horizontal style-form" id="commentForm" >
            @csrf
              <div class="form-group ">
                <label for="cname" class="control-label col-lg-2">Nom du produit (obligatoire)</label>
                <div class="col-lg-10">
                  <input class=" form-control" id="cname" name="nom_produit" minlength="2" type="text" required />
                </div>
              </div>

              <div class="form-group ">
                <label class="control-label col-md-3">Image du produit</label>
                      <div class="col-md-9">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" alt="" />
                          </div>
                          <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                          <div>
                            <span class="btn btn-theme02 btn-file">
                              <span class="fileupload-new"><i class="fa fa-paperclip"></i> Choisir l'image</span>
                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Changer</span>
                            <input type="file" name ="picture[]" class="default" />
                            </span>
                            <a href="advanced_form_components.html#" class="btn btn-theme04 fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Enlever</a>
                          </div>
                        </div>
                        <span class="label label-info">NOTE!</span>
                        <span>
                          Attached image thumbnail is
                          supported in Latest Firefox, Chrome, Opera,
                          Safari and Internet Explorer 10 only
                          </span>

                        </div>
              </div>


              <div class="form-group ">
                <label for="ccomment" class="control-label col-lg-2">Description du produit (obligatoire)</label>
                <div class="col-lg-10">
                  <textarea class="form-control " id="ccomment" name="description_produit" required></textarea>
                </div>
              </div>

              <div class="form-group ">
                <label for="ccomment" class="control-label col-lg-2">Categorie du produit</label>
                <div class="col-lg-10">
                  <select class="form-control" name="categorie">


                  </select>
                </div>
              </div>




              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-theme" type="submit">Valider</button>
                  <button class="btn btn-theme04" type="Cancel">Annuler</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /form-panel -->
      </div>@endsection
