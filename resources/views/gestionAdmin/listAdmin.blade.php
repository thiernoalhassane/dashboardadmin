@extends('template.generique')

@section('contenuHeader')
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">Liste des administrateurs
        </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Liste Administrateurs</li>
        </ol>
        </div><!-- /.col -->
    </div>
@endsection

@section('contenu')
    <div class="row">
        @foreach ($admin as $admin)
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
                <div class="card bg-light d-flex flex-fill">
                <div class="card-header text-muted border-bottom-0">
                    {{$admin->users_type->roles[0]->name}}
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                    <div class="col-7">
                        <h2 class="lead"><b>{{$admin->users_type->firstName}} {{$admin->users_type->lastName}}</b></h2>
                        <p class="text-muted text-sm"><b>Pays: </b> {{$admin->users_type->country->name}}</p>
                        <p class="text-muted text-sm"><b>Phone: </b> {{$admin->users_type->phone}}</p>
                    </div>
                    <div class="col-5 text-center">
                        <img src="../../dist/img/user.png" alt="user-avatar" class="img-circle img-fluid">
                    </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="text-right">
                    <a href="#" class="btn btn-sm btn-danger">
                            Supprimer
                    </a>
                    <a href="#" class="btn btn-sm btn-primary">
                        Changer de  Role
                    </a>

                    </div>
                </div>
                </div>
            </div>
        @endforeach

    </div>

@endsection


