@extends('template.generique')

@section('contenuHeader')
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">Ajouter un administrateur</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Ajouter un  administrateur</li>
        </ol>
        </div><!-- /.col -->
    </div>
@endsection

@section('contenu')
<form action="{{route('addingAdministrateur')}}" method="POST">
    @csrf
<div class="row">
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                <h3 class="card-title">Information Administrateur</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                    </button>
                </div>
                </div>
                <div class="card-body">
                <div class="form-group">
                    <label for="inputName">Nom</label>
                    <input type="text" id="inputName" name="firstName" class="form-control">
                </div>
                <div class="form-group">
                    <label for="inputName">Prenom</label>
                    <input type="text" id="inputName" name="lastName" class="form-control">
                </div>

                <div class="form-group">
                    <label for="inputName">Email</label>
                    <input type="email" id="inputName" name="email" class="form-control">
                </div>

                <div class="form-group">
                    <label for="inputName">Phone</label>
                    <input type="text" name="phone" id="inputName" class="form-control">
                </div>

                <div class="form-group">
                    <label for="inputStatus">Pays</label>
                    <select id="inputStatus" name="country" class="form-control custom-select">
                    <option selected disabled>Choix du pays</option>
                    @foreach ($country as  $country)
                        <option value="{{$country->name}}">{{$country->name}}</option>
                    @endforeach
                    </select>
                </div>
                </div>
                <!-- /.card-body -->
            </div>
        <!-- /.card -->
        </div>
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                <h3 class="card-title">Role</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                    </button>
                </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="inputStatus">Role de l'administrateur</label>
                        <select id="inputStatus" name="role" class="form-control custom-select">
                        <option selected disabled>Choix du role</option>
                        @foreach ($role as  $role)
                                    <option value="{{$role->name}}">{{$role->name}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        <!-- /.card -->
        </div>

        <div class="row">
            <div class="col-12">
            <a href="#" class="btn btn-secondary">Annuler</a>
            <input type="submit" value="Ajouter un Administrateur" class="btn btn-success float-right">
            </div>
        </div>
    </form>

@endsection
