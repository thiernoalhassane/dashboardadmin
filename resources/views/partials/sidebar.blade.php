<div class="sidebar">
    <?php

    $infoUser = Cache::get('infoUser');

    ?>
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="dist/img/user.png" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{$infoUser->firstName}} {{$infoUser->lastName}}</a>
      </div>
    </div>

    <!-- SidebarSearch Form -->


    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
                <a href="{{route('dashboard')}}" class="nav-link">
                  <i class="nav-icon fas fa-home"></i>
                  <p>
                    Accueil
                  </p>
                </a>
        </li>

        <li class="nav-item">
          <a href="{{route('search')}}" class="nav-link">
            <i class="nav-icon fas fa-search"></i>
            <p>
              Recherche
            </p>
          </a>
        </li>
        <li class="nav-header">GESTION ADMINISTRATEURS & ROLES</li>

          <li class="nav-item">
            <a href="{{route('listAdministrateur')}}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>
                Liste Administrateur
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('addAdministrateur')}}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>
                Ajouter Administrateur
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('listRole')}}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>
                Liste Roles
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('listPays')}}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>
                Liste Pays
              </p>
            </a>
          </li>
        <li class="nav-header">GESTION CATEGORIES</li>
          <li class="nav-item">
            <a href="{{route('listCategorie')}}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>
                Liste Categories
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('listSousCategorie')}}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>
                Liste Sous-Categories
              </p>
            </a>
          </li>
        <li class="nav-header">GESTION DES UTILISATEURS</li>
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>
                Liste des Commandes
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('listProduit')}}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>
                Liste des Produits
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('listVendeur')}}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>
                Liste des Vendeurs
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('listUtilisateur')}}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>
                Liste des Utilisateurs
              </p>
            </a>
          </li>
        <li class="nav-header">STATISTIQUES</li>
        <li class="nav-item">
          <a href="{{route('profile')}}" class="nav-link">
            <i class=""></i>
            <p>
              Parametres
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('logout')}}" class="nav-link">
            <i class=""></i>
            <p>
              Logout
            </p>
          </a>
        </li>

      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
