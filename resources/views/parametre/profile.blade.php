@extends('template.generique')

@section('contenuHeader')
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">Profile </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">User Profile</li>
        </ol>
        </div><!-- /.col -->
    </div>
@endsection

@section('contenu')
<div class="row">
    <div class="col-md-3">
    <?php
        $infoUser = Cache::get('infoUser');
        $role = Cache::get('role');

    ?>
      <!-- Profile Image -->
      <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle"
                 src="../../dist/img/user.png"
                 alt="User profile picture">
          </div>

          <h3 class="profile-username text-center">{{$infoUser->firstName}} {{$infoUser->lastName}}</h3>

          <p class="text-muted text-center">{{$role}}</p>

          <ul class="list-group list-group-unbordered mb-3">
            <li class="list-group-item">
              <b>Email</b> <a class="float-right">{{$infoUser->email}}</a>
            </li>
            <li class="list-group-item">
              <b>Phone</b> <a class="float-right">{{$infoUser->phone}}</a>
            </li>
            <li class="list-group-item">
              <b>Pays</b> <a class="float-right">{{$infoUser->country->name}}</a>
            </li>
          </ul>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      <!-- About Me Box -->

      <!-- /.card -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="card">
        <div class="card-header p-2">
          <ul class="nav nav-pills">
            <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Modifier les informations personnels</a></li>
            <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Modifier le password</a></li>
          </ul>
        </div><!-- /.card-header -->
        <div class="card-body">
          <div class="tab-content">
            <div class="active tab-pane" id="activity">
                <form class="form-horizontal" action="{{route('updateInformation')}}" method="POST">
                    @csrf
                    <div class="form-group row">
                      <label for="inputName" class="col-sm-2 col-form-label">Nom</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputName" name="firstName" placeholder="Nom">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail" class="col-sm-2 col-form-label">Prenoms</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail" name="lastName" placeholder="Prenom">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputName2" class="col-sm-2 col-form-label">Email</label>
                      <div class="col-sm-10">
                        <input type="email" name="email" class="form-control" id="inputName2" placeholder="Email">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputExperience" class="col-sm-2 col-form-label">Phone</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail" name="phone" placeholder="Phone">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="offset-sm-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </div>
                  </form>
            </div>
            <!-- /.tab-pane -->
            <!-- /.tab-pane -->

            <div class="tab-pane" id="settings">
              <form class="form-horizontal" action="{{route('updatePassword')}}" method="POST">
                  @csrf
                <div class="form-group row">
                  <label for="inputName" class="col-sm-2 col-form-label">Ancien mot de passe</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputName" name="oldPassword">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Nouveau mot de passe</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail" name="password">
                  </div>
                </div>

                <div class="form-group row">
                  <div class="offset-sm-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div><!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
@endsection









