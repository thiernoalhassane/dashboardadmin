@extends('template.generique')

@section('contenuHeader')
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">Liste des sous categories <span class="badge badge-info right">{{count($subcategorie)}}</span>
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                Ajouter une sous-categorie
            </button>
        </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Liste des sous-categories</li>
        </ol>
        </div><!-- /.col -->
    </div>
@endsection

@section('contenu')
<div class="row">
    <div class="col-12">
      <!-- /.card -->

      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Sous categories</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Nom</th>
              <th>Categorie</th>
              <th>Nombre de produits</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($subcategorie as $subcategorie)
                    <tr>
                        <td>{{$subcategorie->libelle}}</td>
                        <td>{{$subcategorie->category->libelle}}
                        </td>
                        <td></td>
                        <td>
                        <input type="submit" value="Supprimer" class="btn btn-danger ">
                        <input type="submit" value="Mise a jour de la categorie" class="btn btn-primary ">
                        </td>
                    </tr>

                @endforeach

            </tbody>
            <tfoot>
            <tr>
                <th>Nom</th>
                <th>Categorie</th>
                <th>Nombre de produits</th>
                <th>Actions</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
@endsection


<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <form action="{{route('addsubCategorie')}}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Ajout d'une sous-categorie</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputName">Nom de la sous-categorie</label>
                        <input type="text" id="inputName" name="libelle" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="inputStatus">Categorie</label>
                        <select id="inputStatus" name="category" class="form-control custom-select">
                        <option selected disabled>Choix de la categorie</option>
                        @foreach ($categorie as  $categorie)
                            <option value="{{$categorie->libelle}}">{{$categorie->libelle}}</option>
                        @endforeach


                        </select>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">fermer</button>
                <button type="submit" class="btn btn-primary">Ajouter</button>
                </div>
            </div>
        </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@section('js')
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    });
</script>
@endsection





