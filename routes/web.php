<?php

use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\categorie\CategorieController;
use App\Http\Controllers\categorie\SubCategorieController;
use App\Http\Controllers\country\CountryController;
use App\Http\Controllers\login\ConnexionController;
use App\Http\Controllers\product\ProductController;
use App\Http\Controllers\roles\RoleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
    // Ca doit etre l'accueil
})->name('welcome');

Route::post('/login',[ConnexionController::class,'login'])->name('login');
Route::get('/listPays',[CountryController::class,'index'])->name('listPays');
Route::post('/pays',[CountryController::class,'store'])->name('addCountry');
Route::get('/listRole', [RoleController::class,'index'])->name('listRole');;
Route::post('/roles',[RoleController::class,'store'])->name('addRole');
Route::get('/listCategorie', [CategorieController::class,'index'])->name('listCategorie');;
Route::post('/categorie', [CategorieController::class,'store'])->name('addCategorie');;
Route::get('/listSousCategorie', [SubCategorieController::class,'index'])->name('listSousCategorie');;
Route::post('/subcategorie', [SubCategorieController::class,'store'])->name('addsubCategorie');;
Route::get('/listproduit', [ProductController::class,'index'])->name('listProduit');
Route::post('/updatePassword', [ConnexionController::class,'updatePassword'])->name('updatePassword');;
Route::post('/updateInformation', [ConnexionController::class,'updateInformation'])->name('updateInformation');;
Route::get('/addAdministrateur', [AdminController::class,'create'])->name('addAdministrateur');;
Route::post('/addAdministrateur', [AdminController::class,'store'])->name('addingAdministrateur');;
Route::get('/listAdministrateur', [AdminController::class,'index'])->name('listAdministrateur');
Route::get('/logout', [ConnexionController::class,'logout'])->name('logout');;
Route::get('/dashboard', function () {
    return view('home.home');
    // Ca doit etre l'accueil
})->name('dashboard');


Route::get('/imageProduit', function () {
    return view('gestionAdmin.addProduit');
    // Ca doit etre l'accueil
})->name('imageProduit');

Route::post('/roles',[ProductController::class,'adding'])->name('addingProduct');







Route::get('/search', function () {
    return view('search.search');
    // Ca doit etre l'accueil
})->name('search');;

Route::get('/listUtilisateur', function () {
    return view('gestionUtilisateurs.listUtilisateur');
    // Ca doit etre l'accueil
})->name('listUtilisateur');;

Route::get('/listvendeur', function () {
    return view('gestionUtilisateurs.listVendeur');
    // Ca doit etre l'accueil
})->name('listVendeur');;



Route::get('/profile', function () {
    return view('parametre.profile');
    // Ca doit etre l'accueil
})->name('profile');
