<?php

namespace App\Http\Controllers\login;

use App\Http\Controllers\Controller;
use App\Traits\RestApi;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class ConnexionController extends Controller
{
    use RestApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request)
    {
        $url = $this->loginurl();
        $form_request = [
            'phone'=> $request["phone"],
            'password'=>$request["passord"]
        ];

            $response = Http::acceptJson()->post($url,[
                'phone'=> $request["phone"],
                'password'=>$request["password"]
            ]);
            $res = json_decode($response->body()) ;

            if ($res->status == "success") {
                Cache::put('token', $res->data->token);
                Cache::put('infoUser', $res->data->user);
                Cache::put('role', $res->data->role[0]);
                return redirect()->to('dashboard');
            } else {
                return redirect()->back();
            }
    }

    public function updatePassword(Request $request)
    {
        $url = $this->updateUserPassword();
            $token = Cache::get('token');
            $response = Http::withToken($token)->acceptJson()->post($url,[
                'password'=> $request["password"],
                'oldPassword'=>$request["oldPassword"]
            ]);
            $res = json_decode($response->body()) ;


            if ($res->status == "success") {
                return redirect()->to('profile');
            } else {
                return redirect()->back();
            }

    }

    public function updateInformation(Request $request)
    {
        $url = $this->updateUserInformation();
        $infoUser =Cache::get('infoUser');
        $form_request= [
                'firstName'=> $request["firstName"],
                'lastName'=>$request["lastName"],
                'email'=>$request["email"],
                'phone'=>$request["phone"],
                //'user'=>$infoUser->id
                'user'=>2
        ];
        $response = Http::acceptJson()->post($url,$form_request);

        $res = json_decode($response->body()) ;



            if ($res->status == "success") {
                Cache::forget('infoUser');
                Cache::put('infoUser', $res->data->user);
                return redirect()->to('profile');
            } else {
                return redirect()->back();
            }

    }



    public function logout()
    {
        $url = $this->getLogout();
            $token = Cache::get('token');
            $response = Http::withToken($token)->acceptJson()->post($url);
            $res = json_decode($response->body()) ;

            if ($res->status == "success") {
                Cache::forget('token');
                Cache::forget('infoUser');
                Cache::forget('role');
                return redirect()->to('/');
            } else {
                return redirect()->back();
            }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
