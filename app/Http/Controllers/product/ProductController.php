<?php

namespace App\Http\Controllers\product;

use App\Http\Controllers\Controller;
use App\Traits\RestApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request as FacadesRequest;

class ProductController extends Controller
{
    use RestApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = $this->getDistinctProduct();
        $response = Http::acceptJson()->get($url);
        $res = json_decode($response->body()) ;
        $product= $res->data;
        return view('gestionUtilisateurs.listProduit',compact('product'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $url = $this->addsubCategorie();
        $form_request = [
            'libelle'=> $request["libelle"],
            'category'=> $request["category"],
        ];

            $response = Http::acceptJson()->post($url,$form_request);
            $res = json_decode($response->body()) ;

            if ($res->status == "success") {
                return redirect()->to('listSousCategorie');
            } else {
                return redirect()->back();
            }
    }

    public function adding(Request $request)
    {
        $url = $this->productaddingurl();

        $pictures = [];
        foreach ($request->file('picture') as $file) {
            if (file_exists($file)) {
                $picture = $file->getClientOriginalName();

                $pictures[] = $picture;



            }
        }

        $img[] = $request->file('picture');

        $form_request = [
            'subCategory'=> [0],
        ];



        $response = Http::attach('attachment',file_get_contents($img[0][0]->getContent()),'img.jpg')
                    ->post($url);
                $res = json_decode($response->body()) ;
                dd($res);




//            if ($res->status == "success") {
  //              return redirect()->to('listSousCategorie');
    //        } else {
      //          return redirect()->back();
        //    }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
