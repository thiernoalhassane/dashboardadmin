<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Traits\RestApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AdminController extends Controller
{
    use RestApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = $this->getListAdmin();
        $response = Http::acceptJson()->get($url);
        $res = json_decode($response->body()) ;
        $admin= $res->data;

        return view('gestionAdmin.listAdmin',compact('admin'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $url = $this->getCountry();
        $url2 = $this->getRoles();
        $response = Http::acceptJson()->get($url);
        $response2 = Http::acceptJson()->get($url2);
        $res = json_decode($response->body()) ;
        $res2 = json_decode($response2->body()) ;
        $country= $res->data;
        $role= $res2->data;
        return view('gestionAdmin.addAdmin',compact('country','role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $url = $this->addingAdmin();
        $form_request = [
            'firstName'=> $request["firstName"],
            'lastName'=>$request["lastName"],
            'email'=>$request["email"],
            'phone'=>$request["phone"],
            'country'=>$request["country"],
            'role'=>$request["role"]
        ];

            $response = Http::acceptJson()->post($url,$form_request);
            $res = json_decode($response->body()) ;


            if ($res->status == "success") {
                return redirect()->to('addAdministrateur');
            } else {
                return redirect()->back();
            }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
