<?php

namespace App\Traits;

use GuzzleHttp\Client;

trait RestApi
{

    protected function url()
    {
        $url = "http://192.168.1.100:8000/api/v1/";
        return $url;
    }

    protected function loginurl()
    {
        $lien = 'administrators/login';
        $url = $this->url().$lien;
        return $url;
    }

    protected function productaddingurl()
    {
        $lien = 'seller/product/add';
        $url = $this->url().$lien;
        return $url;
    }



    protected function getCountry()
    {
        $lien = 'administrators/country/all';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addCountry()
    {
        $lien = 'administrators/country/add';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getRoles()
    {
        $lien = 'administrators/roles/all';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addRoles()
    {
        $lien = 'administrators/roles/add';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getCategorie()
    {
        $lien = 'administrators/categories/all';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addCategorie()
    {
        $lien = 'administrators/categories/add';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getsubCategorie()
    {
        $lien = 'administrators/subcategories/all';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addsubCategorie()
    {
        $lien = 'administrators/subcategories/add';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getDistinctProduct()
    {
        $lien = 'product/getDistinctProduct';
        $url = $this->url().$lien;
        return $url;
    }

    protected function updateUserPassword()
    {
        $lien = 'updatePassword';
        $url = $this->url().$lien;
        return $url;
    }

    protected function updateUserInformation()
    {
        $lien = 'updateInformation';
        $url = $this->url().$lien;
        return $url;
    }



    protected function addingAdmin()
    {
        $lien = 'administrators/admin/add';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getListAdmin()
    {
        $lien = 'administrators/admin/all';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getLogout()
    {
        $lien = 'logout';
        $url = $this->url().$lien;
        return $url;
    }







}
